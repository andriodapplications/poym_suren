package com.example.kanthi.projectmonitoring.Dashboard;

import android.graphics.Path;

import java.util.ArrayList;
import java.util.HashMap;

public interface GetPaths {
    void polygonPaths(ArrayList<Path> path, HashMap<Path, Integer> mPathsMap);
}
